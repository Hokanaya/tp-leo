# TP1 : Configuration système


# Sommaire

* [0. Préliminaires](#0-préliminaires)
* [I. Utilisateurs](#i-utilisateurs)
    * [1. Création et configuration](#1-création-et-configuration)
    * [2. SSH](#2-ssh)
* [II. Configuration réseau](#ii-configuration-réseau)
    * [1. Nom de domaine](#1-nom-de-domaine)
    * [2. Serveur DNS](#2-serveur-dns)
* [III. Partitionnement](#iii-partitionnement)
    * [1. Préparation de la VM](#1-préparation-de-la-vm)
    * [2. Partitionnement](#2-partitionnement)
* [IV. Gestion de services](#iv-gestion-de-services)
    * [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
    * [2. Création de service](#2-création-de-service)
        * [A. Unité simpliste](#a-unité-simpliste)
        * [B. Modification de l'unité](#b-modification-de-lunité)


# 0.Préliminaires

## Installation 'Vim'

```bash
Sudo yum install vim
```

# I-Utilisateurs
	
	## 1-création-et-configuration

### Création d'un utilisateur avec son répertoire home précisé et son répertoire Shell dans /bin/bash

```bash
Useradd toto -b /home -s /bin/bash
```

### Création d'un groupe

```bash
groupadd admins
```

### Ajout du groupe admins à l'accès à la commande 'sudo'

```bash
Visudo
```

**Résultat :**

```
[root@vm1 srv]# cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
##
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhaps using
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool

## Installation and management of software
# Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
# Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig, /usr/bin/systemctl start, /usr/bin/systemctl stop, /usr/bin/systemctl reload, /usr/bin/systemctl restart, /usr/bin/systemctl status, /usr/bin/systemctl enable, /usr/bin/systemctl disable

## Updating the locate database
# Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
# Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp

## Processes
# Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
# Cmnd_Alias DRIVERS = /sbin/modprobe

# Defaults specification

#
# Refuse to run if unable to disable echo on the tty.
#
Defaults   !visiblepw

#
# Preserving HOME has security implications since many programs
# use it when searching for configuration files. Note that HOME
# is already set when the the env_reset option is enabled, so
# this option is only effective for configurations where either
# env_reset is disabled or HOME is present in the env_keep list.
#
Defaults    always_set_home
Defaults    match_group_by_gid

# Prior to version 1.8.15, groups listed in sudoers that were not
# found in the system group database were passed to the group
# plugin, if any. Starting with 1.8.15, only groups of the form
# %:group are resolved via the group plugin by default.
# We enable always_query_group_plugin to restore old behavior.
# Disable this option for new behavior.
Defaults    always_query_group_plugin

Defaults    env_reset
Defaults    env_keep =  "COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS"
Defaults    env_keep += "MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
Defaults    env_keep += "LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
Defaults    env_keep += "LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
Defaults    env_keep += "LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"

#
# Adding HOME to env_keep may enable a user to run unrestricted
# commands via sudo.
#
# Defaults   env_keep += "HOME"

Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

## Next comes the main part: which users can run what software on
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
##      user    MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL

## Allows members of the 'sys' group to run networking, software,
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
%admins  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL

## Allows members of the users group to mount and unmount the
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d
```

### Ajout de l'utilisateur toto au groupe admins

```bash
adduser toto admins
```


# II. configuration-réseau

## 1. Nom de domaine

```bash
hostname vm1.tp3.b1
```

## 2. Serveur DNS

```bash
[root@localhost ~]# [root@localhost ~]# cat /etc/resolv.conf
# Generated by NetworkManager
search localdomain
nameserver 1.1.1.1
```

# III. Partitionnement

## 2. Partitionnement

Agréger les deux volumes

```bash

[root@vm1 ~]# pvcreate /dev/sdd
  Physical volume "/dev/sdd" successfully created.
[root@vm1 ~]# pvcreate /dev/sde
  Physical volume "/dev/sde" successfully created.
  
[root@vm1 ~]#vgcreate DATA /dev/ssd /dev/sde
   Volume  group "DATA" successfully created
```

créer 3 logical volumes de 2 Go chacun

```bash[root@vm1 ~]# lvcreate -L 2G Vol1 -n DATA
  Volume group "Vol1" not found
  Cannot process volume group Vol1
[root@vm1 ~]# lvcreate -L 2G DATA -n Vol1
  Logical volume "Vol1" created.
[root@vm1 ~]# lvcreate -L 2G DATA -n Vol2
  Logical volume "Vol2" created.
[root@vm1 ~]# lvcreate -L 2G DATA -n Vol3
  Volume group "DATA" has insufficient free space (510 extents): 512 required.
[root@vm1 ~]# lvcreate -l 100%FREE DATA -n Vol3
  Logical volume "Vol3" created.
```

formater ces partitions en ext4


```bash
[root@vm1 dev]# mkfs -t ext4 /dev/DATA/Vol1
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

```bash
[root@vm1 dev]# mkfs -t ext4 /dev/DATA/Vol2
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

```bash
[root@vm1 dev]# mkfs -t ext4 /dev/DATA/Vol3
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
130560 inodes, 522240 blocks
26112 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=534773760
16 block groups
32768 blocks per group, 32768 fragments per group
8160 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```


monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.

```bash
[root@vm1 ~]# df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 899M     0  899M   0% /dev
tmpfs                    910M     0  910M   0% /dev/shm
tmpfs                    910M  9.6M  901M   2% /run
tmpfs                    910M     0  910M   0% /sys/fs/cgroup
/dev/mapper/centos-root   50G  1.5G   49G   3% /
/dev/sda1               1014M  195M  820M  20% /boot
/dev/mapper/centos-home   97G   33M   97G   1% /home
/dev/mapper/DATA-Vol1    2.0G  6.0M  1.8G   1% /mnt/part1
/dev/mapper/DATA-Vol3    2.0G  6.0M  1.9G   1% /mnt/part3
/dev/mapper/DATA-Vol2    2.0G  6.0M  1.8G   1% /mnt/part2
tmpfs                    182M     0  182M   0% /run/user/0
```


# IV. Gestion de services

## 1. Interaction avec un service existant

```bash
systemctl status firewalld
firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2021-01-05 10:55:52 CET; 3h 17min ago
     Docs: man:firewalld(1)
 Main PID: 765 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─765 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid
```

## 2.Creation de service

```bash
[root@localhost ~]# systemctl start web
[root@localhost ~]# systemctl enable web
Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.
```

